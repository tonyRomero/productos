import './App.css';

import React, { useEffect, useState } from 'react';
import { getProductsList } from './services/productos';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { Row, Col } from 'react-bootstrap';

import CardActions from '@material-ui/core/CardActions';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const [list, setList] = useState([]);

  const [data, setData] = useState({});

  const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 648,
    },
    media: {
      height: 0,
      paddingTop: '56.25%',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));

  const classes = useStyles();

  useEffect(() => {
    let mounted = true;
    getProductsList()
      .then(items => {
        if (mounted) {
          setList(items)
        }
        setData(items[0])
      })
    return () => mounted = false;
  }, [])

  return (

    <Container>

      <Row>


        <h1>PRODUCTOS</h1>
        <Col sm={4}>
          {list.map(item =>
            <Card className={classes.root} key={item.id}>

              <CardMedia onClick={() => setData(item)}
                className={classes.media}
                image={item.image}
              />

              <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                  {item.title}
                </Typography>
              </CardContent>

              <CardActions disableSpacing>
                <CreditCardIcon /> <h3>$ {item.price}</h3>

              </CardActions>
            </Card>)}
        </Col>
        <Col sm={8}>

          <Container>

            <Card className={classes.root}>
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe" className={classes.avatar}>
                    { data.id }
                  </Avatar>
                }
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
                title={ data.category }
                subheader={'$' + data.price }
              />
              <CardMedia
                className={classes.media}
                image={data.image}
                title={ data.title }
              />
              <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                {data.title}
                </Typography>
              </CardContent>
            </Card>
          </Container>

        </Col>
      </Row>
    </Container>
  );
}

export default App;