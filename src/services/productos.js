/**
 * Se crea un archivo de servicios para hacer las peticiones de productos, se intentó implementar
 */

const BASE_URL = 'https://fakestoreapi.com/';

export function getProductsList() {
    return fetch(`${BASE_URL}products`)
      .then(data => data.json())
  }

  export function getProductById(id) {
    return fetch(`${BASE_URL}products/${id}`)
      .then(data => data.json())
  }